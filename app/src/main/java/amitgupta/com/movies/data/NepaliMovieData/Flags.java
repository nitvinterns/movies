
package amitgupta.com.movies.data.NepaliMovieData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flags {

    @SerializedName("purchase")
    @Expose
    private Integer purchase;
    @SerializedName("expired")
    @Expose
    private Integer expired;

    /**
     * 
     * @return
     *     The purchase
     */
    public Integer getPurchase() {
        return purchase;
    }

    /**
     * 
     * @param purchase
     *     The purchase
     */
    public void setPurchase(Integer purchase) {
        this.purchase = purchase;
    }

    /**
     * 
     * @return
     *     The expired
     */
    public Integer getExpired() {
        return expired;
    }

    /**
     * 
     * @param expired
     *     The expired
     */
    public void setExpired(Integer expired) {
        this.expired = expired;
    }

}
