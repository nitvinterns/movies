
package amitgupta.com.movies.data.NepaliMovieData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainMovieData {

    @SerializedName("general")
    @Expose
    private General general;
    @SerializedName("movies")
    @Expose
    private List<Movie> movies = null;

    /**
     * 
     * @return
     *     The general
     */
    public General getGeneral() {
        return general;
    }

    /**
     * 
     * @param general
     *     The general
     */
    public void setGeneral(General general) {
        this.general = general;
    }

    /**
     * 
     * @return
     *     The movies
     */
    public List<Movie> getMovies() {
        return movies;
    }

    /**
     * 
     * @param movies
     *     The movies
     */
    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

}
