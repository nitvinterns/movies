
package amitgupta.com.movies.data.NepaliMovieData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class General {

    @SerializedName("CurrentTimestampUrl")
    @Expose
    private String currentTimestampUrl;

    /**
     * 
     * @return
     *     The currentTimestampUrl
     */
    public String getCurrentTimestampUrl() {
        return currentTimestampUrl;
    }

    /**
     * 
     * @param currentTimestampUrl
     *     The CurrentTimestampUrl
     */
    public void setCurrentTimestampUrl(String currentTimestampUrl) {
        this.currentTimestampUrl = currentTimestampUrl;
    }

}
