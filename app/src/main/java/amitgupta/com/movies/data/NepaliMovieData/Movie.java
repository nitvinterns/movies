
package amitgupta.com.movies.data.NepaliMovieData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Movie {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("movie_genre")
    @Expose
    private String movieGenre;
    @SerializedName("movie_name")
    @Expose
    private String movieName;
    @SerializedName("movie_duration")
    @Expose
    private String movieDuration;
    @SerializedName("movie_sub_category")
    @Expose
    private String movieSubCategory;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("video_stream_type")
    @Expose
    private String videoStreamType;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("directors")
    @Expose
    private String directors;
    @SerializedName("actors")
    @Expose
    private String actors;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("stream_url")
    @Expose
    private String streamUrl;
    @SerializedName("price_tag")
    @Expose
    private String priceTag;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("flags")
    @Expose
    private Flags flags;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The movieGenre
     */
    public String getMovieGenre() {
        return movieGenre;
    }

    /**
     * 
     * @param movieGenre
     *     The movie_genre
     */
    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }

    /**
     * 
     * @return
     *     The movieName
     */
    public String getMovieName() {
        return movieName;
    }

    /**
     * 
     * @param movieName
     *     The movie_name
     */
    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    /**
     * 
     * @return
     *     The movieDuration
     */
    public String getMovieDuration() {
        return movieDuration;
    }

    /**
     * 
     * @param movieDuration
     *     The movie_duration
     */
    public void setMovieDuration(String movieDuration) {
        this.movieDuration = movieDuration;
    }

    /**
     * 
     * @return
     *     The movieSubCategory
     */
    public String getMovieSubCategory() {
        return movieSubCategory;
    }

    /**
     * 
     * @param movieSubCategory
     *     The movie_sub_category
     */
    public void setMovieSubCategory(String movieSubCategory) {
        this.movieSubCategory = movieSubCategory;
    }

    /**
     * 
     * @return
     *     The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * 
     * @param categoryName
     *     The category_name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * 
     * @return
     *     The videoStreamType
     */
    public String getVideoStreamType() {
        return videoStreamType;
    }

    /**
     * 
     * @param videoStreamType
     *     The video_stream_type
     */
    public void setVideoStreamType(String videoStreamType) {
        this.videoStreamType = videoStreamType;
    }

    /**
     * 
     * @return
     *     The about
     */
    public String getAbout() {
        return about;
    }

    /**
     * 
     * @param about
     *     The about
     */
    public void setAbout(String about) {
        this.about = about;
    }

    /**
     * 
     * @return
     *     The directors
     */
    public String getDirectors() {
        return directors;
    }

    /**
     * 
     * @param directors
     *     The directors
     */
    public void setDirectors(String directors) {
        this.directors = directors;
    }

    /**
     * 
     * @return
     *     The actors
     */
    public String getActors() {
        return actors;
    }

    /**
     * 
     * @param actors
     *     The actors
     */
    public void setActors(String actors) {
        this.actors = actors;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The streamUrl
     */
    public String getStreamUrl() {
        return streamUrl;
    }

    /**
     * 
     * @param streamUrl
     *     The stream_url
     */
    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    /**
     * 
     * @return
     *     The priceTag
     */
    public String getPriceTag() {
        return priceTag;
    }

    /**
     * 
     * @param priceTag
     *     The price_tag
     */
    public void setPriceTag(String priceTag) {
        this.priceTag = priceTag;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The flags
     */
    public Flags getFlags() {
        return flags;
    }

    /**
     * 
     * @param flags
     *     The flags
     */
    public void setFlags(Flags flags) {
        this.flags = flags;
    }

}
