package amitgupta.com.movies.fragments.moviesfragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import amitgupta.com.movies.R;

/**
 * Created by dell on 12/12/2016.
 */

public class NepaliMovieFragment extends Fragment {

    public NepaliMovieFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //Link The XML and The FRAGMENT
        return inflater.inflate(R.layout.fragment_movie_nepali,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
