package amitgupta.com.movies.adapters.recyclerviewadapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import amitgupta.com.movies.R;
import amitgupta.com.movies.data.NepaliMovieData.Movie;


/**
 * Created by dell on 12/8/2016.
 */

public class RecyclerViewAdapterHorizontal extends RecyclerView.Adapter<RecyclerViewAdapterHorizontal.MyViewHolder> {

    List<Movie> horizontalList;
    Context context;

    public RecyclerViewAdapterHorizontal(List<Movie> horizontalList, Context context){
        this.horizontalList=horizontalList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //1.Link To the XML
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_vedio_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        //3.Decide what happens when button is clicked based on the ViewHolder
        holder.movieName.setText(horizontalList.get(position).getMovieName());

        Picasso.with(context).load(horizontalList.get(position).getImage()).into(holder.movieImage);

        holder.movieImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,holder.movieName.getText().toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    //Inner class created to Hold the View
    public class MyViewHolder extends RecyclerView.ViewHolder{

        // 2.Link XML to View(text,buttons etc)
        TextView movieName;
        ImageView movieImage;
        public MyViewHolder(View itemView) {
            super(itemView);
            movieName= (TextView) itemView.findViewById(R.id.movie_name);
            movieImage= (ImageView) itemView.findViewById(R.id.movie_image);
        }
    }
}
