package amitgupta.com.movies.adapters.recyclerviewadapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import amitgupta.com.movies.R;
import amitgupta.com.movies.data.NepaliMovieData.Movie;


/**
 * Created by dell on 12/8/2016.
 */

public class RecyclerViewAdapterVertical extends RecyclerView.Adapter<RecyclerViewAdapterVertical.MyViewHolder>{

    ArrayList<String> verticalList;
    List<Movie> horizontalList;
    Context context;
    public RecyclerViewAdapterVertical(ArrayList<String> verticalList, List<Movie> horizontalList, Context context){
        this.verticalList=verticalList;
        this.horizontalList=horizontalList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //1.Link To the XML
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.horizontal_vedio_list, parent, false);

        //Pass to MyViewHolder to link to the Views(Buttons,texts)
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.movieTopic.setText(verticalList.get(position));

        holder.movieTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,holder.movieTopic.getText().toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView movieTopic;
        RecyclerView recyclerViewHorizontal;
        public MyViewHolder(View itemView) {
            super(itemView);
            recyclerViewHorizontal= (RecyclerView) itemView.findViewById(R.id.recycleview_horiontal);
            movieTopic= (TextView) itemView.findViewById(R.id.movie_topic);

            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            recyclerViewHorizontal.setLayoutManager(horizontalLayoutManagaer);

            RecyclerViewAdapterHorizontal recyclerViewAdapterHorizontal = new RecyclerViewAdapterHorizontal(horizontalList,context);
            recyclerViewHorizontal.setAdapter(recyclerViewAdapterHorizontal);
        }
    }


}
