package amitgupta.com.movies.tablayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import amitgupta.com.movies.R;
import amitgupta.com.movies.adapters.viewpager.ViewPagerAdapter;
import amitgupta.com.movies.fragments.moviesfragment.AsianMovieFragment;
import amitgupta.com.movies.fragments.moviesfragment.EnglishMovieFragment;
import amitgupta.com.movies.fragments.moviesfragment.HindiMovieFragment;
import amitgupta.com.movies.fragments.moviesfragment.NepaliMovieFragment;

/**
 * Created by dell on 12/12/2016.
 */

public class MovieTabLayoutActivity extends AppCompatActivity {
    
    Toolbar movieToolBar;
    TabLayout movieTabs;
    ViewPager movieViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        setContentView(R.layout.activity_movie_tab_layout);
       
        movieTabs= (TabLayout) findViewById(R.id.movie_tabs);
        movieViewPager= (ViewPager) findViewById(R.id.movie_view_pager);
        
        //For toolbar,TabLayout and ViewPager
        setupViewPager(movieViewPager);

        //Now LINK the ViewPager with TabView , so that we get what we want to see
        // FragmentsAdapter-ViewPagerAdapter-ViewPager-tabs
        movieTabs.setupWithViewPager(movieViewPager);

    }

    //Try View view as the parameter
    private void setupViewPager(ViewPager view) {
        //First Create the AdapterObject
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Add the Fragments and its titles to ViewPagerAdapter
        viewPagerAdapter.addFragments(new NepaliMovieFragment(),"Nepali Movies");
        viewPagerAdapter.addFragments(new EnglishMovieFragment(),"English Movies");
        viewPagerAdapter.addFragments(new HindiMovieFragment(),"Hindi Movies");
        viewPagerAdapter.addFragments(new AsianMovieFragment(),"Asian Movies");

        //Connect the ADAPTER and The View
        view.setAdapter(viewPagerAdapter);

    }
}
