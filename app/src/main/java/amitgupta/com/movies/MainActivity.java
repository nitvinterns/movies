package amitgupta.com.movies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import amitgupta.com.movies.tablayout.MovieTabLayoutActivity;

//RecyclerView Branch
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //To go TabView/TabLayout Activity
        Intent intent = new Intent(MainActivity.this, MovieTabLayoutActivity.class);
        //Here Use Bundle to Pass Values between Activity
        startActivity(intent);

    }
}
